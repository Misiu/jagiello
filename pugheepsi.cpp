#include <pthread.h>
#include <time.h> 
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>     /* srand, rand */
#include <ncurses.h>
#define STUDENTS 20

#include <sstream>
#include <string.h>
using namespace std;



struct Student
{
	int Cash;
	int ID;
	
	int Position;
};

struct Lady{
	//4-5-drukarka //1-3 - komputer
	int From;
	int Target; //4-5-drukarka //1-3 - komputer
};

int timer;
int printers[2];
int computers[3];
bool TheEnd=false;
Lady lady;
Student students[STUDENTS];
int queue[STUDENTS];
pthread_t students_t[STUDENTS];
pthread_t lady_t;
pthread_mutex_t computers_m[3];
pthread_mutex_t printers_m[2]{
    PTHREAD_MUTEX_INITIALIZER,
    PTHREAD_MUTEX_INITIALIZER
};
pthread_mutex_t lady_m[2];
pthread_cond_t printing[2];

int whoFirst(){
	for(int i=0; i<STUDENTS; i++){
		if(queue[i] != -1)
			return queue[i];	
	}
	return -1;
}


void printBoard(){
	/*
	Godzina: HH:MM
	Drukarka I: compX 	Drukarka II: compY
	Kobieta: skad -> dokad
	Komputer I: stA
	Komputer II: stB
	Komputer III: stC

	W kolejce: st1, st2, st3,...
	*/
	int hours=timer / 60;
	int minetes=timer % 60;
	//system ( "clear" );
	cout<<"Godzina: "<<hours<<":"<<minetes<<"\n";


	//cout<<"Drukark I: "<<printers[0]+1<<"("<<students[computers[printers[0]]].ID+1<<")"<<"\tDrukarka II: "<<printers[1]+1<<"("<<students[computers[printers[1]]].ID+1<<")"<<"\n";
	cout<<"Drukark I: "<<printers[0]+1<<"\tDrukarka II: "<<printers[1]+1<<"\n";
	cout<<"Kobieta z drukarki "<<lady.From+1<<" do komputera "<<lady.Target+1<<"\n";

	cout<<"Komputer I: "<<computers[0]+1<<"\n";
	cout<<"Komputer II: "<<computers[1]+1<<"\n";
	cout<<"Komputer III: "<<computers[2]+1<<"\n";
}


// 'the' means p_thread function

void* theStudent(void *arg)
{
	struct Student *info=(struct Student*)arg;
	bool living = true;
int j=0;
	while(!TheEnd && living){
		for(int i=0; i<3; i++){
			if(info->ID==whoFirst()){
				if(pthread_mutex_trylock(&computers_m[i])==0){

					computers[i]=info->ID;
					queue[info->ID]=-1;
					info->Position=i;
						
					sleep(rand()%4+2);
					
					
cout<<"\t\t\t\t\tstudent "<<info->ID+1<<" czeka na drukarke "<<endl;
					
					while(pthread_mutex_trylock(&printers_m[j])!=0){
					//cout<<"\t\t\t\t\tstudent "<<info->ID<<" probuje "<<j<<endl;
cout<<"\t\t\t\t\tstudent "<<info->ID+1<<" nie mógł zająć "<<j+1<<" drukarki "<<" -- ma ją: "<<printers[j]+1<<endl;
						j++;
						//j=j%2;
						j=1;
sleep(1);

					}
					printers[j]=info->Position;
cout<<"\t\t\t\t\tstudent "<<info->ID+1<<" zajal drukarke "<<j<<endl;
					//cout<<"\t\t\t\t\tstudent "<<info->ID<<" zajalem "<<info->Position<<endl;
					//pthread_cond_wait(&printing[j],  &printers_m[j]);
					pthread_mutex_trylock(&lady_m[j]);
					//printf("\t\t\t\t\t zajalem Lady\n");
					pthread_cond_wait(&printing[j],  &lady_m[j]);
					cout<<"\t\t\t\t\tstudent "<<info->ID+1<<" zbiera misie "<<j<<endl;
					printers[j]=-1;
//cout<<info->ID<<" wydrykował\n";
					
					pthread_mutex_unlock(&lady_m[j]);
					computers[info->Position]=-1;

					if(pthread_mutex_unlock(&printers_m[j])==0){
					cout<<"\t\t\t\t\tstudent "<<info->ID+1<<" odblokowal  drukarke "<<j<<endl;
					}else{
					cout<<"\t\t\t\t\tstudent "<<info->ID+1<<" dblokowal NIE drukarke "<<j<<endl;
					}
					pthread_mutex_unlock(&computers_m[info->Position]);					
					living=false;
					break;
				}
			}
		}
	}
//int zajecie=pthread_mutex_trylock(&printers_m[j]);
//if(zajecie!=0)
//printf("\t\t\t\t\t\t\tDUPA!!");
//pthread_mutex_unlock(&printers_m[j]);
cout<<"\t\t\t\t\tstudent "<<info->ID+1<<" umiera "<<endl;
}

void* theLady(void* arg)
{	
	struct Lady *info=(struct Lady*)arg;
	int i=0;
	while(!TheEnd){
		if(printers[i]!=-1)
		if(pthread_mutex_trylock(&lady_m[i])==0){
//		if(printers[i]!=-1){
			info->From=i;
			info->Target=printers[i];
			sleep(2);
			
			printf("\t\t\t\t\tDoszłam z %d drukarki\n", printers[i]);
			info->From=-1;
			info->Target=-1;
			pthread_mutex_unlock(&lady_m[i]);
			pthread_cond_broadcast(&printing[i]);
			sleep(1);
		}
		i++;
		i=i%2;
	}
}

void* theStudentsFactory(void* arg) // taki akademik
{
	
	int err;
	
	for(int i=0; i<STUDENTS; i++){
		
		//sleep(rand()%3 );
		sleep(2);
		students[i].ID=i;
		

		if(!(err=pthread_create(&students_t[i], NULL, &theStudent, &students[i]))){
			//printf("Rozmnożenie studentów o %d\n", i);
			queue[i]=i;
		}else
		{
			printf("wystapil blad przy tworzeniu watku: %d\n", err);
		}
	}




}






int end;
int x = 0,y = 0;
int max_x = 0,max_y = 0;

int mj_rys, mj_posx, mj_posy;
int mz_posx_init, mz_posx, mz_posy;
int oj_posx, oj_posy;
int bp_posx, bp_posy, bm_posx, bm_posy;
const char* str;
string komp1, komp2, komp3, print1, print2, ladys, hour_s, min_s;
string lady_from, lady_target;
int main()
{
	/* initialize random seed: */
	srand (time(NULL));
	timer = 540;
	lady.From=-1;
	lady.Target=-1;
	int err;
	
	for(int i=0; i< STUDENTS; i++){
		queue[i]=-1;
	}
	for(int i=0; i< 3; i++){
		computers[i]=-1;
	}
	for(int i=0; i< 2; i++){
		printers[i]=-1;
		pthread_mutex_init(&printers_m[i], NULL);
		printing[i]= PTHREAD_COND_INITIALIZER;
		pthread_cond_init(&printing[i], NULL);	
	}
	pthread_t studentsFactory_t;

	if(!(err=pthread_create(&lady_t, NULL, &theLady, &lady))){
		printf("Narodziny Venus\n");
	}else
	{
		printf("wystapil blad przy tworzeniu watku: %d\n", err);
	}

	if(!(err=pthread_create(&studentsFactory_t, NULL, &theStudentsFactory, NULL))){
		printf("Otwarto akademik\n");
	}else
	{
		printf("wystapil blad przy tworzeniu watku: %d\n", err);
	}

/*



	initscr();
  	curs_set(FALSE);

	getmaxyx(stdscr, max_y, max_x);
 
  	x = max_x / 2 - 10;
  	y = max_y / 2;

	mj_rys = 0;
	mj_posx = x+31;
	mj_posy = y;
	mz_posx_init = x+2;
	mz_posx = mz_posx_init;
	mz_posy = y-7;
	bp_posx = x-3;
	bp_posy = y+6;
	bm_posx = x-3;
	bm_posy = y+7;*/

	while(!TheEnd)	//wywolywanie co 2 sekundy obrazu sytuacji 
	{
/*	int hours=timer / 60;
	int minetes=timer % 60;
	//pthread_mutex_lock(&mutex_rysuj);

//char *str;
//sprintf(str, "%d", printers[0]);
		hour_s = to_string(hours);
		const char * hour = hour_s.c_str();
		min_s = to_string(minetes);
		const char * min = min_s.c_str();
		mvprintw(y-12, x-2,hour);
		mvprintw(y-12, x,":");
		mvprintw(y-12, x+1,min);
		
		lady_from = to_string(lady.From+1);
		const char * lady_from_c = lady_from.c_str();
		lady_target = to_string(lady.Target+1);
		const char * lady_target_c = lady_target.c_str();

		mvprintw(y-4, x+8, "|LADY|");
		mvprintw(y-3, x+8, lady_from_c);
		mvprintw(y-3, x+10, "->");
		mvprintw(y-3, x+12, lady_target_c);
		komp1 = to_string(computers[0]+1);
		const char * comp1 = komp1.c_str();
		komp2 = to_string(computers[1]+1);
		const char * comp2 = komp2.c_str();
		komp3 = to_string(computers[2]+1);
		const char * comp3 = komp3.c_str();

		print1 = to_string(students[computers[printers[0]]].ID+1);
		const char * printer1 = komp1.c_str();
		print2 = to_string(students[computers[printers[1]]].ID+1);
		const char * printer2 = komp2.c_str();


		mvprintw(y-8, x-10, "DRUKARKA1");
		mvprintw(y-8, x+10, "DRUKARKA2");


		mvprintw(y, x-25, "KOMPUTER1");
		mvprintw(y, x-5, "KOMPUTER2");
		mvprintw(y, x+15, "KOMPUTER3");


		mvprintw(y+1, x-23,comp1);
		mvprintw(y+1, x-3,comp2);
		mvprintw(y+1, x+20,comp3);


		mvprintw(y-7, x-8,printer1);
		mvprintw(y-7, x+12,printer2);

		//mvprintw(y-2, x+8, hours);

*/
	sleep(2);

	//system ( "clear" );
	//cout<<"Godzina: "<<hours<<":"<<minetes<<"\n";
//	cout<<"Drukark I, komp: "<<printers[0]+1<<"("<<students[computers[printers[0]]].ID+1<<")"<<"\tDrukarka II, komp: "<<printers[1]+1<<"("<<students[computers[printers[1]]].ID+1<<")"<<"\n";
	//cout<<"Kobieta z drukarki "<<lady.From+1<<" do komputera "<<lady.Target+1<<"\n";
//cout<<"Komputer I: "<<computers[0]+1<<"\n";
//	cout<<"Komputer II: "<<computers[1]+1<<"\n";
//	cout<<"Komputer III: "<<computers[2]+1<<"\n";

		printBoard();
		timer += 10;
		refresh();
	//	pthread_mutex_unlock(&mutex_rysuj);
		
	

	}

	return 0;
}








